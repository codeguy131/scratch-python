"""practice problems from https://realpython.com/python-practice-problems/"""


def add_it_up(i):
    """Add all numbers up to and including i"""

    try:
        res = sum(range(i + 1))
        return print(res)
    except KeyError:
        res = 0
        return res


if __name__ == "__main__":
    add_it_up(int(input("Enter number: ")))
