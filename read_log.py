"""Playing with logging and asyncio module"""
import logging
from asyncio import create_task, sleep, run

# Log config
logging.basicConfig(
    filename="./info.log",
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s: %(message)s",
)


def log_stuff():
    """Trying out logging module"""

    logging.info("Hello from logger")


async def aprint(uin):
    """Async print function"""
    await sleep(0.002)
    logging.info(uin)
    return print(uin)


async def main():
    """Creates tasks for printing text to log"""

    task1 = create_task(aprint("Hello from task 1"))
    task2 = create_task(aprint("Hello from task 2"))

    # Await on tasks
    await task1
    await task2

    print("All done")


if __name__ == "__main__":
    log_stuff()
    run(main())
