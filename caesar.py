"""Practice from https://realpython.com/python-practice-problems/"""
import string


def encode_data(msg, shift=1):
    """Encode data using caesar cipher"""

    l_case = string.ascii_lowercase
    mask = l_case[shift:] + l_case[:shift]
    tra = str.maketrans(l_case, mask)
    return print(msg.translate(tra))


if __name__ == "__main__":
    encode_data(str(input("Enter text: ")))
